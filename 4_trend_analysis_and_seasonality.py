import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose

# Read the preprocessed CSV file / gold_data
gold_data = pd.read_csv("preprocessed_gold_data.csv")

# Convert the 'Date' column to datetime format
gold_data['Date'] = pd.to_datetime(gold_data['Date'])

# Perform seasonal analysis using statsmodels lib because of higher performance
result = seasonal_decompose(gold_data['Close'], model='multiplicative', period=365)

# Plot the gold time series with seasonality and moving average in a 30-day window
plt.figure(figsize=(16, 10))
plt.subplot(4, 1, 1)
plt.plot(gold_data['Date'], gold_data['Close'], color='blue', label='Observed')
moving_average = gold_data['Close'].rolling(window=30).mean()
plt.plot(gold_data['Date'], moving_average, color='orange', label='Moving Average (30-day)')
plt.ylabel("Price ($)")

# Connecting the first and last moving average elements to see overall rise/fall
plt.plot([gold_data['Date'].values[29], gold_data['Date'].values[-1]], [moving_average.values[29], moving_average.values[-1]], color='black', label='Overall')
plt.legend()

plt.subplot(4, 1, 2)
plt.plot(gold_data['Date'], result.trend, color='orange', label='Trend')
plt.ylabel("Trend")
plt.legend()

plt.subplot(4, 1, 3)
plt.plot(gold_data['Date'], result.seasonal, color='green', label='Seasonality')
plt.ylabel("Seasonality")
plt.legend()

plt.subplot(4, 1, 4)
plt.plot(gold_data['Date'], result.resid, color='red', label='Residuals')
plt.ylabel("Residuals")
plt.xlabel("Date")
plt.legend()


plt.suptitle("Gold Price Seasonality")
plt.tight_layout()
plt.savefig("gold_seasonality.png")
plt.close()


# Read the preprocessed CSV file / bitcoin_data
bitcoin_data = pd.read_csv("preprocessed_bitcoin_data.csv")

# Convert the 'Date' column to datetime format
bitcoin_data['Date'] = pd.to_datetime(bitcoin_data['Date'])

# Perform seasonal analysis using statsmodels lib because of higher performance
result = seasonal_decompose(bitcoin_data['Close'], model='multiplicative', period=365)

# Plot the bitcoin time series with seasonality and moving average in a 30-day window
plt.figure(figsize=(16, 10))
plt.subplot(4, 1, 1)
plt.plot(bitcoin_data['Date'], bitcoin_data['Close'], color='blue', label='Observed')
moving_average = bitcoin_data['Close'].rolling(window=30).mean()
plt.plot(bitcoin_data['Date'], moving_average, color='orange', label='Moving Average (30-day)')
plt.ylabel("Price ($)")

# Connecting the first and last moving average elements to see overall rise/fall
plt.plot([bitcoin_data['Date'].values[29], bitcoin_data['Date'].values[-1]], [moving_average.values[29], moving_average.values[-1]], color='black', label='Overall')
plt.legend()

plt.subplot(4, 1, 2)
plt.plot(bitcoin_data['Date'], result.trend, color='orange', label='Trend')
plt.ylabel("Trend")
plt.legend()

plt.subplot(4, 1, 3)
plt.plot(bitcoin_data['Date'], result.seasonal, color='green', label='Seasonality')
plt.ylabel("Seasonality")
plt.legend()

plt.subplot(4, 1, 4)
plt.plot(bitcoin_data['Date'], result.resid, color='red', label='Residuals')
plt.ylabel("Residuals")
plt.xlabel("Date")
plt.legend()

plt.suptitle("Bitcoin Price Seasonality")
plt.tight_layout()
plt.savefig("bitcoin_seasonality.png")
plt.close()

import pandas as pd

# Read the CSV files
gold_data = pd.read_csv("gold_data.csv")
bitcoin_data = pd.read_csv("bitcoin_data.csv")

# Check and correct date column format
gold_data['Date'] = pd.to_datetime(gold_data['Date']).dt.date
bitcoin_data['Date'] = pd.to_datetime(bitcoin_data['Date']).dt.date

# Replace nulls with forward values
gold_data.fillna(method='ffill', inplace=True)
bitcoin_data.fillna(method='ffill', inplace=True)

# Normalize the data
gold_data['Normalized_Price'] = (gold_data['Close'] - gold_data['Close'].min()) / (gold_data['Close'].max() - gold_data['Close'].min())
bitcoin_data['Normalized_Price'] = (bitcoin_data['Close'] - bitcoin_data['Close'].min()) / (bitcoin_data['Close'].max() - bitcoin_data['Close'].min())

# Stationary transformation
gold_data['Stationary_Price'] = gold_data['Normalized_Price'].diff()
bitcoin_data['Stationary_Price'] = bitcoin_data['Normalized_Price'].diff()

# Save the preprocessed data
gold_data.to_csv("preprocessed_gold_data.csv", index=False)
bitcoin_data.to_csv("preprocessed_bitcoin_data.csv", index=False)

# Show a small sample of each dataset
print("Gold Data Sample:")
print(gold_data.head())
print()
print("Bitcoin Data Sample:")
print(bitcoin_data.head())

import yfinance as yf
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

# List of items
items = ['USDEUR=X', 'USDSAR=X', 'USDCNY=X', 'BTC-USD', 'TNX', 'GC=F', 'HG=F', 'ZC=F']

results = {}

# Download data for each item and perform PCA
for item in items:
    # Download data
    data = yf.download(item, start='2011-01-01', end='2021-12-31')
    data = data.ffill()

    # Normalize
    scaler = MinMaxScaler()
    normalized_data = scaler.fit_transform(data)

    # Perform PCA
    pca = PCA()
    principal_components = pca.fit_transform(normalized_data)

    # Get explained variance ratio
    explained_variance_ratio = pca.explained_variance_ratio_

    # Plot Scree Chart
    plt.figure(figsize=(10, 6))
    plt.plot(range(1, len(explained_variance_ratio) + 1), explained_variance_ratio, marker='o')
    plt.xlabel('Principal Components')
    plt.ylabel('Explained Variance Ratio')
    plt.title(f'Scree Chart - {item}')
    plt.grid(True)
    plt.savefig(f"{item}_scree_chart.png")
    plt.close()

    # Biplot
    plt.figure(figsize=(10, 8))
    plt.scatter(principal_components[:, 0], principal_components[:, 1])
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')
    plt.title(f'Biplot - {item}')
    plt.grid(True)

    # Add feature names
    features = data.columns
    for i, feature in enumerate(features):
        plt.annotate(feature, (pca.components_[0, i], pca.components_[1, i]))

    plt.savefig(f"{item}_biplot.png")
    plt.close()

    # Store results
    results[item] = {
        'explained_variance_ratio': explained_variance_ratio,
        'principal_components': principal_components
    }

    print(f"PCA on {item} data: Done.")
print("Principal Component Analysis (PCA) has been performed on the provided data.")

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import correlate


# 1. Price plots


# Read the preprocessed CSV file / gold_data
gold_data = pd.read_csv('preprocessed_gold_data.csv')

# Convert date column to datetime format
gold_data['Date'] = pd.to_datetime(gold_data['Date'])

# Plot gold price
plt.figure(figsize=(12, 6))
plt.plot(gold_data['Date'], gold_data['Close'], label='Gold', color='gold')
plt.xlabel('Date')
plt.ylabel('Price ($)')
plt.title('Gold Price')
plt.legend()
plt.savefig("gold_plot.png")
plt.close()

# Read the preprocessed CSV file / bitcoin_data
bitcoin_data = pd.read_csv('preprocessed_bitcoin_data.csv')

# Convert date column to datetime format
bitcoin_data['Date'] = pd.to_datetime(bitcoin_data['Date'])

# Plot bitcoin price
plt.figure(figsize=(12, 6))
plt.plot(bitcoin_data['Date'], bitcoin_data['Close'], label='Bitcoin', color='orange')
plt.xlabel('Date')
plt.ylabel('Price ($)')
plt.title('Bitcoin Price')
plt.legend()
plt.savefig("bitcoin_plot.png")
plt.close()


# 2. Statistical attributes


# Statistical attributes of gold time series
gold_mean = gold_data['Close'].mean()
gold_variance = gold_data['Close'].var()
gold_median = gold_data['Close'].median()
gold_min = gold_data['Close'].min()
gold_max = gold_data['Close'].max()
gold_std = gold_data['Close'].std()

# Print statistical properties of gold time series
print("Gold Time Series Statistical Properties:")
print(f"Mean: {gold_mean}")
print(f"Variance: {gold_variance}")
print(f"Median: {gold_median}")
print(f"Minimum: {gold_min}")
print(f"Maximum: {gold_max}")
print(f"Standard Deviation: {gold_std}")

# Statistical attributes of bitcoin time series
bitcoin_mean = bitcoin_data['Close'].mean()
bitcoin_variance = bitcoin_data['Close'].var()
bitcoin_median = bitcoin_data['Close'].median()
bitcoin_min = bitcoin_data['Close'].min()
bitcoin_max = bitcoin_data['Close'].max()
bitcoin_std = bitcoin_data['Close'].std()

# Print statistical properties of gold time series
print("\nBitcoin Time Series Statistical Properties:")
print(f"Mean: {bitcoin_mean}")
print(f"Variance: {bitcoin_variance}")
print(f"Median: {bitcoin_median}")
print(f"Minimum: {bitcoin_min}")
print(f"Maximum: {bitcoin_max}")
print(f"Standard Deviation: {bitcoin_std}")


# 3. Correlation and Causality


# Cross-correlation between gold and bitcoin prices
gold_close = gold_data['Close'].values
bitcoin_close = bitcoin_data['Close'].values

cross_correlation = correlate(gold_close, bitcoin_close)

# Plot cross-correlation
plt.figure(figsize=(12, 6))
plt.plot(np.arange(len(cross_correlation)) - len(gold_close) + 1, cross_correlation, color='blue')
plt.axhline(0, linestyle='--', color='red')
plt.xlabel('Lag')
plt.ylabel('Cross-Correlation')
plt.title('Cross-Correlation between Gold and Bitcoin Prices')
plt.savefig("cross_correlation.png")
plt.close()

# Calculate correlation between different parameters of gold and bitcoin prices
gold_close = gold_data['Close'].values
gold_open = gold_data['Open'].values
gold_high = gold_data['High'].values
gold_low = gold_data['Low'].values

bitcoin_close = bitcoin_data['Close'].values
bitcoin_open = bitcoin_data['Open'].values
bitcoin_high = bitcoin_data['High'].values
bitcoin_low = bitcoin_data['Low'].values

gold_correlation_matrix = np.corrcoef([gold_close, gold_open, gold_high, gold_low])
bitcoin_correlation_matrix = np.corrcoef([bitcoin_close, bitcoin_open, bitcoin_high, bitcoin_low])

# Plot correlation matrix for gold
plt.figure(figsize=(8, 6))
plt.imshow(gold_correlation_matrix, cmap='coolwarm', interpolation='none')
plt.colorbar()
plt.xticks(np.arange(4), ['Close', 'Open', 'High', 'Low'])
plt.yticks(np.arange(4), ['Close', 'Open', 'High', 'Low'])
plt.title('Correlation Matrix for Gold Prices')
plt.savefig("gold_correlation_matrix.png")
plt.close()

# Plot correlation matrix for bitcoin
plt.figure(figsize=(8, 6))
plt.imshow(bitcoin_correlation_matrix, cmap='coolwarm', interpolation='none')
plt.colorbar()
plt.xticks(np.arange(4), ['Close', 'Open', 'High', 'Low'])
plt.yticks(np.arange(4), ['Close', 'Open', 'High', 'Low'])
plt.title('Correlation Matrix for Bitcoin Prices')
plt.savefig("bitcoin_correlation_matrix.png")
plt.close()

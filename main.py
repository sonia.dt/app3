"""
define the variables:

x1 = amount of product 1 made in time period 1
x2 = amount of product 2 made in time period 1
x3 = amount of product 3 made in time period 1
x4 = amount of product 1 made in time period 2
x5 = amount of product 2 made in time period 2
x6 = amount of product 3 made in time period 2

define the objective function:

maximize interest = (interest of product 1 * x1) + (interest of product 2 * x2) + (interest of product 3 * x3)
         + (interest of product 1 * x4) + (interest of product 2 * x5) + (interest of product 3 * x6)

define the limitations:

1. Machine 1 time limitation:
   (time to produce product 1 with machine 1 * x1) + (time to produce product 2 with machine 1 * x2) + (time to produce product 3 with machine 1 * x3)
   <= maximum time for machine 1 in time period 1

   (time to produce product 1 with machine 1 * x4) + (time to produce product 2 with machine 1 * x5) + (time to produce product 3 with machine 1 * x6)
   <= maximum time for machine 1 in time period 2

2. Machine 2 time limitation:
   (time to produce product 1 with machine 2 * x1) + (time to produce product 2 with machine 2 * x2) + (time to produce product 3 with machine 2 * x3)
   <= maximum time for machine 2 in time period 1

   (time to produce product 1 with machine 2 * x4) + (time to produce product 2 with machine 2 * x5) + (time to produce product 3 with machine 2 * x6)
   <= maximum time for machine 2 in time period 2

3. Worker limitation:
   (number of workers needed for product 1 * x1) + (number of workers needed for product 2 * x2) + (number of workers needed for product 3 * x3)
   <= total number of workers in time period 1

   (number of workers needed for product 1 * x4) + (number of workers needed for product 2 * x5) + (number of workers needed for product 3 * x6)
   <= total number of workers in time period 2

4. Storage limitation:
   x1 + x4 <= storage for product 1
   x2 + x5 <= storage for product 2
   x3 + x6 <= storage for product 3

5. Demand limitation:
   x1 >= predicted demand for product 1 in time period 1
   x2 >= predicted demand for product 2 in time period 1
   x3 >= predicted demand for product 3 in time period 1
   x4 >= predicted demand for product 1 in time period 2
   x5 >= predicted demand for product 2 in time period 2
   x6 >= predicted demand for product 3 in time period 2

"""
from scipy.optimize import linprog

# values are set
interest_product1 = 1.7
interest_product2 = 1.8
interest_product3 = 1.6

time_product1_machine1 = 10
time_product1_machine2 = 12

time_product2_machine1 = 13
time_product2_machine2 = 16

time_product3_machine1 = 8
time_product3_machine2 = 10

workers_product1 = 2
workers_product2 = 3
workers_product3 = 2

max_time_machine1_period1 = 300
max_time_machine2_period1 = 250

total_workers_period1 = 50
total_workers_period2 = 40

storage_product1 = 35
storage_product2 = 30
storage_product3 = 40

predicted_demand_product1_period1 = 3
predicted_demand_product2_period1 = 4
predicted_demand_product3_period1 = 4
predicted_demand_product1_period2 = 2
predicted_demand_product2_period2 = 1
predicted_demand_product3_period2 = 2

# Define the objective function coefficients
c = [-interest_product1, -interest_product2, -interest_product3, -interest_product1, -interest_product2, -interest_product3]

# Define the constraints matrix
A = [[time_product1_machine1, time_product2_machine1, time_product3_machine1, 0, 0, 0],
     [0, 0, 0, time_product1_machine2, time_product2_machine2, time_product3_machine2],
     [workers_product1, workers_product2, workers_product3, 0, 0, 0],
     [0, 0, 0, workers_product1, workers_product2, workers_product3],
     [-1, 0, 0, -1, 0, 0],
     [0, -1, 0, 0, -1, 0],
     [0, 0, -1, 0, 0, -1]]

# Define the constraints inequality right hand side
b = [max_time_machine1_period1, max_time_machine2_period1, total_workers_period1, total_workers_period2, storage_product1, storage_product2, storage_product3]

# Define the bounds for the decision variables (positive)
bounds = [(predicted_demand_product1_period1, None), (predicted_demand_product2_period1, None), (predicted_demand_product3_period1, None),
          (predicted_demand_product1_period2, None), (predicted_demand_product2_period2, None), (predicted_demand_product3_period2, None)]

# Solve with linear-programming
result = linprog(c, A_ub=A, b_ub=b, bounds=bounds)

# Results
if result.success:
    print("Optimal Solution:")
    print("Product 1 - Period 1: ", result.x[0])
    print("Product 2 - Period 1: ", result.x[1])
    print("Product 3 - Period 1: ", result.x[2])
    print("Product 1 - Period 2: ", result.x[3])
    print("Product 2 - Period 2: ", result.x[4])
    print("Product 3 - Period 2: ", result.x[5])
    print("Maximized Interest: ", -result.fun)
else:
    print("Optimization failed. Please check the constraints.")
# At the end we have the "Maximized Interest"

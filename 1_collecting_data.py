import yfinance as yf
from datetime import datetime, timedelta

# Past 10 years
end_date = datetime.now().strftime('%Y-%m-%d')
start_date = (datetime.now() - timedelta(days=10 * 365)).strftime('%Y-%m-%d')

# Download Bitcoin data from yf
btc_data = yf.download('BTC-USD', start=start_date, end=end_date)
btc_data.to_csv('bitcoin_data.csv')

# Download gold data from yf
gold_data = yf.download('GC=F', start=start_date, end=end_date)
gold_data.to_csv('gold_data.csv')
